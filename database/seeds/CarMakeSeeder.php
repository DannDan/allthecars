<?php

use Illuminate\Database\Seeder;

class CarMakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CarMake::class, 50)->create()->each(function ($carMake) {
            $carMake->carModels()->createMany(factory(App\CarModel::class,mt_rand(3,7))->make()->toArray());
        });
    }
}
