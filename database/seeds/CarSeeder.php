<?php

use App\CarModel;
use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = CarModel::with('carMake')->get();

        foreach ($models as $model) {
            $makeName = $model->carMake->name;
            $modelName = $model->name;

            $model->cars()->createMany(factory(App\Car::class,mt_rand(2,5))->make(['name' => $makeName." ".$modelName])->toArray());
        }
    }
}
