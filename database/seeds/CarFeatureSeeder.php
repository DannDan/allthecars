<?php

use App\Car;
use App\CarFeature;
use Faker\Provider\CarData;
use Faker\Provider\Fakecar;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $props = CarData::getVehicleProperties();
        $propArray = [];
        foreach ($props as $value) {
            $feature = new CarFeature();
            $feature->name = $value;
            $feature->price = mt_rand(100,1500);
            $feature->save();
            $propArray[$value] = $feature->id;
        }
        $faker = Faker\Factory::create();
        $faker->addProvider(new Fakecar($faker));

        $cars = Car::all();

        foreach ($cars as $car) {

            $fakePropsArray = $faker->vehicleProperties;
            
            foreach ($fakePropsArray as $fakeProp) {
                $fakeId = $propArray[$fakeProp];
                DB::table('car_feature')->insert([
                    'car_id' => $car->id, 
                    'car_feature_id' => $fakeId
                    ]
                );
            }
            
        }
    }
}
