<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_makes', function(Blueprint $table)
        {
            $table->index('name');
        });
        Schema::table('car_models', function(Blueprint $table)
        {
            $table->index('name');
        });
        Schema::table('cars', function(Blueprint $table)
        {
            $table->index('fuel_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_makes', function(Blueprint $table)
        {
            $table->dropIndex(['name']);
        });
        Schema::table('car_models', function(Blueprint $table)
        {
            $table->dropIndex(['name']);
        });
        Schema::table('cars', function(Blueprint $table)
        {
            $table->dropIndex(['fuel_type']);
        });
    }
}
