<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\CarMake;
use Faker\Provider\Fakecar;
use Faker\Generator as Faker;

$factory->define(CarMake::class, function (Faker $faker) {
    $faker->addProvider(new Fakecar($faker));
    return [
        'name' => $faker->unique()->vehicleBrand
    ];
});
