<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Car;
use App\Model;
use Faker\Provider\Fakecar;
use Faker\Generator as Faker;

$factory->define(Car::class, function (Faker $faker) {
    $faker->addProvider(new Fakecar($faker));
    $fuelType = $faker->vehicleFuelType;
    if($fuelType == 'gas'){
        $fuelType = 'petrol';
    }
    return [
        'name' => $faker->vehicle,
        'registration' => $faker->vehicleRegistration,
        'vin' => $faker->vin,
        'type' => $faker->vehicleType,
        'fuel_type' => $fuelType,
        'mileage' => mt_rand(10,500000)
    ];
});
