<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\CarModel;
use Faker\Provider\Fakecar;
use Faker\Generator as Faker;

$factory->define(CarModel::class, function (Faker $faker) {
    $faker->addProvider(new Fakecar($faker));
    return [
        'name' => $faker->vehicleModel
    ];
});
