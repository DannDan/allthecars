## Documentation  


### Setup

Run the following commands in project directory:

`composer install`  
`npm install`  
`npm run dev`  

create local database and update .env file with dbname dbuser dbpass

`php artisan key:generate`   
`php artisan migrate`  
`php artisan db:seed`  
`php artisan setup:user`  copy the token given into config/app.php to replace the token already there, keyed as 'bearer_token'

You can use this token in postman as 'Bearer your_token' in the authentication header. You may need to add header to request application/json, 'Accept: application/json' as the default is not that. 

### Endpoints

#### GET /api/v1/makes
#### GET /api/v1/makes/{make}/models
#### GET /api/v1/search?make=Audi&model=TT&fuelType=Petrol

All endpoints are public but protected by a bearer token.

### App

Open application in browser where you can search for cars by make, model,fuel type, features and feature cost. and order by make model mileage. make sure you have the token in the app.php config file