<?php

namespace App\Repositories;

use App\CarModel;
use App\Repositories\Interfaces\CarModelRepositoryInterface;
use App\Http\Resources\Collections\CarModelCollectionResource;


class CarModelRepository implements CarModelRepositoryInterface
{
    public function allForMake($carMake) : CarModelCollectionResource
    {
        return new CarModelCollectionResource($carMake->carModels);
    }
}