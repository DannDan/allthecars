<?php

namespace App\Repositories\Interfaces;

use App\Http\Resources\Collections\CarMakeCollectionResource;

interface CarMakeRepositoryInterface
{
    public function all() : CarMakeCollectionResource;
}