<?php

namespace App\Repositories\Interfaces;

use App\Http\Resources\Collections\CarCollectionResource;

interface CarRepositoryInterface
{
    public function search($carMake, $carModel, $fuelType, $features, $featureCost, $orderBy, $order) : CarCollectionResource;
}