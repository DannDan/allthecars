<?php

namespace App\Repositories\Interfaces;

use App\Http\Resources\Collections\CarModelCollectionResource;

interface CarModelRepositoryInterface
{
    public function allForMake($carMake) : CarModelCollectionResource;
}