<?php

namespace App\Repositories;

use App\Car;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\CarResource;
use App\Repositories\Interfaces\CarRepositoryInterface;
use App\Http\Resources\Collections\CarCollectionResource;


class CarRepository implements CarRepositoryInterface
{
    public function search($carMake, $carModel, $fuelType, $features, $featureCost, $orderBy, $order) : CarCollectionResource
    {
        
        $query = DB::table('cars')->join('car_models','cars.car_model_id', '=', 'car_models.id')->join('car_makes','car_models.car_make_id', '=', 'car_makes.id')
        ->leftJoin('car_feature','cars.id','=','car_feature.car_id')->leftJoin('car_features', 'car_feature.car_feature_id','car_features.id')->groupBy('cars.id');
        if(!empty($carMake)){
            $query = $query->where('car_makes.name', '=' , $carMake);
        }
        if(!empty($carModel)){
            $query = $query->where('car_models.name', '=' , $carModel);
        }
        if(!empty($fuelType)){
            $query = $query->where('cars.fuel_type', '=' , $fuelType);
        }
        if(!empty($features)){
            if(!is_array($features)){
                $features = explode(",", $features);
            }
            $query = $query->whereIn('car_features.name', $features);
        }

        if(!empty($featureCost)){
            $query = $query->havingRaw('SUM(car_features.price) > ?', [$featureCost]);
        }
        

        if(!empty($orderBy)){
           $query = $this->orderBySwitch($query, $orderBy, $order); 
        }

        $cars = $this->addResourceSelect($query)->get();

        return new CarCollectionResource($cars);
    }

    private function addResourceSelect($query){
        return $query->select('cars.*','car_makes.name as makeName', 'car_models.name as modelName',DB::raw('GROUP_CONCAT(car_features.name) as featureList'));
    }

    private function orderBySwitch($query, $orderBy, $order){

        $order = $order == null ? 'asc' : $order; 

        switch ($orderBy) {
            case 'make':
                return $query->orderBy('car_makes.name', $order);
                break;
            case 'model':
                return $query->orderBy('car_models.name', $order);
                break;
            case 'mileage':
                return $query->orderBy('cars.mileage', $order);
                break;
            
            default:
                return $query;
            break;
        }
        
    }
}