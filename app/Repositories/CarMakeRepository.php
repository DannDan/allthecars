<?php

namespace App\Repositories;

use App\CarMake;
use App\Repositories\Interfaces\CarMakeRepositoryInterface;
use App\Http\Resources\Collections\CarMakeCollectionResource;

class CarMakeRepository implements CarMakeRepositoryInterface
{
    public function all() : CarMakeCollectionResource
    {
        return new CarMakeCollectionResource(CarMake::orderBy('name')->get());
    }
}