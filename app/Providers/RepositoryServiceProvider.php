<?php

namespace App\Providers;

use App\Repositories\CarRepository;
use App\Repositories\CarMakeRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\CarModelRepository;
use App\Repositories\Interfaces\CarRepositoryInterface;
use App\Repositories\Interfaces\CarMakeRepositoryInterface;
use App\Repositories\Interfaces\CarModelRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CarMakeRepositoryInterface::class, 
            CarMakeRepository::class
        );
        $this->app->bind(
            CarModelRepositoryInterface::class, 
            CarModelRepository::class
        );
        $this->app->bind(
            CarRepositoryInterface::class, 
            CarRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
