<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarFeature extends Model
{
    public function cars(){
        return $this->belongsToMany('App\Car', 'car_feature', 'car_feature_id','car_id' );
    }
}
