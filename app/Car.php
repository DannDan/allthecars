<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function carModel()
    {
        return $this->belongsTo('App\CarModel');
    }

    public function carFeatures(){
        return $this->belongsToMany('App\CarFeature', 'car_feature', 'car_id', 'car_feature_id');
    }
}
