<?php

namespace App\Http\Resources;

use App\Http\Resources\CarModelResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'model' => $this->modelName,
            'make' => $this->makeName,
            'registration' => $this->registration,
            'vin' => $this->vin,
            'type' => $this->type,
            'fuelType' => $this->fuel_type,
            'mileage' => $this->mileage,
            'features' => $this->featureList == null ? [] : explode(",",$this->featureList)
        ];
    }
}
