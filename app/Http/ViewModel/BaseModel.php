<?php
namespace App\Http\ViewModel;

abstract class BaseModel {

    public function toJson(){
        $props = get_object_vars($this);
        $encodedString = json_encode($props);
        $replaceString = str_replace("'"," ",$encodedString);
        return $replaceString;
    }
}