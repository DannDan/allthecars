<?php
namespace App\Http\ViewModel;

use App\CarFeature;
use Illuminate\Support\Facades\URL;
use App\Http\Resources\Collections\CarFeatureCollectionResource;

class HomeViewModel extends BaseModel{

    public $apiUrl = '';
    public $token = '';
    public $features = [];

    function __construct(){
        $this->apiUrl = URL::to('/api/v1');
        $this->token = config('app.bearer_token');
        $this->features = new CarFeatureCollectionResource(CarFeature::all());
    }
}