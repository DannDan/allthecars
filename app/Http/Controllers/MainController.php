<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\ViewModel\HomeViewModel;

class MainController extends Controller
{
    public function index(Request $request){

        $vm = new HomeViewModel();
        return view('home',['vm'=> $vm->toJson()]);
    }
}
