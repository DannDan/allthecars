<?php

namespace App\Http\Controllers\Api;

use App\CarMake;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Collections\CarMakeCollectionResource;
use App\Http\Resources\Collections\CarModelCollectionResource;
use App\Repositories\Interfaces\CarMakeRepositoryInterface;
use App\Repositories\Interfaces\CarModelRepositoryInterface;

class MakesController extends Controller
{

    private $carMakeRepository;
    private $carModelRepository;

    public function __construct(CarMakeRepositoryInterface $carMakeRepository, CarModelRepositoryInterface $carModelRepository)
    {
        $this->carMakeRepository = $carMakeRepository;
        $this->carModelRepository = $carModelRepository;
    }

    public function all(Request $request) : CarMakeCollectionResource{
        return $this->carMakeRepository->all();
    }

    public function modelsForMake(CarMake $carMake) : CarModelCollectionResource{
        return $this->carModelRepository->allForMake($carMake);
    }
}
