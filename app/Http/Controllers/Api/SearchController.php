<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CarRepositoryInterface;
use App\Http\Resources\Collections\CarCollectionResource;
use App\Repositories\Interfaces\CarMakeRepositoryInterface;
use App\Repositories\Interfaces\CarModelRepositoryInterface;

class SearchController extends Controller
{

    private $carMakeRepository;
    private $carModelRepository;
    private $carRepository;

    public function __construct(CarRepositoryInterface $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    public function search(Request $request) : CarCollectionResource{

        // $request->validate([
        //     'make' => 'required'
        // ]);

        $cars = $this->carRepository->search(
            $request->input('make'), 
            $request->input('model'), 
            $request->input('fuelType'), 
            $request->input('features'),
            $request->input('featureCost'),
            $request->input('orderBy'),
            $request->input('order')
        );

        return $cars;
    }
}
