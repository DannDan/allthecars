<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    public function carMake()
    {
        return $this->belongsTo('App\CarMake');
    }
    public function cars()
    {
        return $this->hasMany('App\Car');
    }
}
