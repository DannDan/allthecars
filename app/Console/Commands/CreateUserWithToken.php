<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Artisan;

class CreateUserWithToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a user with a access token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = new User();
        $user->password = Hash::make('the-password-of-choice');
        $user->email = 'email-doesnt-matter@for-this.com';
        $user->name = 'Random Name';
        $user->save();
        $token = $user->createToken('bearer-token');
        $this->info($token->plainTextToken);
        
        return 0;
    }
}
