import axios from 'axios';
import store from '../store'

class Api{

    constructor(){
        this.apiUrl = ''
        this.token = ''
        this.http = null
    }
    createClient (url, bearer){
        this.apiUrl = url;
        this.token = bearer;
        this.http = axios.create(
            {
                baseURL: url,
                headers: {
                    "Authorization" : "Bearer " + bearer
                }
            }
        )
        this.http.interceptors.response.use(response => {
            return response;
         }, error => {
           
            this.checkError(error.response);
           
           return error;
         });

    }
    async getAllMakes (cb) {
      this.http.get(
        '/makes'
        ).then((response) => {

            if(response.status == 200 && response.data.data != null){
                cb(response.data.data);
            }
        })
        .catch((error) => {
            
        });
    }
    async getAllModelsForMake(makeName,cb) {
        this.http.get(
            '/makes/'+makeName+'/models'
        ).then((response) => {
            if(response.status == 200 && response.data.data != null){
                cb(response.data.data);
            }
        })
        .catch((error) => {
           
        });
    }
    async search(search,cb) {
        this.http.get(
            '/search',
            {
                params: search
            }
        ).then((response) => {
            if(response.status == 200 && response.data.data != null){
                cb(response.data.data);
            }
        })
        .catch((error) => {
           
        });
    }
    checkError(error){
        if(error.status == 401){
           store.dispatch('setAuth', false) 
           return
        }
        if(error.status == 422){
            store.dispatch('reset')
            return
         }
         store.dispatch('reset')
    }
  
  }

  const apiInstance = new Api()

  export default apiInstance;