window._ = require('lodash');
import Vue from 'vue';
import store from './store';

Vue.config.debug = true

Vue.component('base-component', require('./components/App.vue').default);

new Vue({
    el: '#app',
    store
});
