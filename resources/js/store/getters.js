export const makes = state => state.makes

export const models = state => state.models

export const cars = state => state.cars

export const features = state => state.features == null ? [] : state.features

export const isAuth = state => state.auth

export const isSearching = state => state.searching

export const isFormLoading = state => state.formLoading
