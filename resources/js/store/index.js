import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const state = {
  auth: true,
  searching:false,
  formLoading:false,
  cars: [
/*
    {
        'name',
        'make',
        'model',
        'registration',
        'vin',
        'type',
        'fuelType',
        'mileage',
        'features'
    }
*/
  ],
  makes: [
    /*
     {
      name
    }
    */
  ],
  models: [
    /*
     {
      name
    }
    */
  ],
  features: [
    /*
    {
      name,
      price
    }
    */
  ]
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
})