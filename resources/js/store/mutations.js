export default {
  setFeatures (state,features){
    features.forEach(feature => {
      state.features.push(feature);
    })
  },
  receiveMakes (state, makes) {
    makes.forEach(make => {
      addMake(state, make)
    })
  },

  receiveModels (state, models) {
      models.forEach(model => {
          addModel(state, model)
      })
    
  },
  receiveCars (state, cars) {
    cars.forEach(car => {
        addCar(state, car)
    })
  
    },
    clearCars (state){
        state.cars = []
      },
  clearModels (state){
    state.models = []
  },
  setAuth (state, auth) {
    state.auth = auth
  },
  setFormLoading (state, formLoading){
      state.formLoading = formLoading
  },
  setSearching (state, searching){
    state.searching = searching
    }

}

function addCar (state, car) {
    state.cars.push(car)
  }

function addModel (state, model) {
  state.models.push(model)
}

function addMake (state, make) {
    state.makes.push(make)
}

