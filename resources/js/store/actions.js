import api from '../api'

export const setAuth = ({ commit }, payload) => {
  commit('setAuth', payload)
}
export const reset = ({ commit }) => {
    
    commit('clearCars')
    commit('setFormLoading', false)
    commit('setSearching', false)
  }

  export const setFeatures = ({ commit },features) => {
    commit('setFeatures', features)
  
}

export const getAllMakes = ({ commit }) => {
    commit('setFormLoading', true)
  api.getAllMakes(makes => {


    commit('receiveMakes', makes)
    commit('setFormLoading', false)
  })
}
export const getModelsForMake = ({ commit }, make) => {
    commit('setFormLoading', true)
    commit('clearModels')
    api.getAllModelsForMake(make.name, models => {
        commit('receiveModels', models)
        commit('setFormLoading', false)
      })
}
export const clearModels = ({ commit }) => {
    commit('clearModels')
}
export const search = ({ commit },search) => {

    commit('setSearching', true)
    commit('clearCars')

    if(search.model == null){
        delete search.model
    }
    if(search.fuelType == null){
        delete search.fuelType
    }
    if(search.orderBy == null){
      delete search.orderBy
    }
    if(search.order == null){
      delete search.order
    }
    if(search.features == null){
      delete search.features
    }
    if(search.featureCost == 0){
      delete search.featureCost
    }
    api.search(search, cars => {
        commit('receiveCars', cars)
        commit('setSearching', false)
      })
}
